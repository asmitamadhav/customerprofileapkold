package com.jumpinjumpout.www.customerprofile;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by asmita on 13-05-2015.
 */
public class UploadToServer {

    private final static String TAG = "UploadToServer";
    public static String pk;
    public void uploadData(final Context context, String msg, final String url, final HashMap<String, String>hmp)
    {
//
//        final ProgressDialog pd = new ProgressDialog(context);
//        pd.setMessage(msg); //
//        pd.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (!response.trim().equals("-1")) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                pk = jsonObject.getString("driver_detail_id");
                                Log.d(TAG,"PK "+pk);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else{

                        }
//                        if ( pd != null)
//                        {
//                            pd.dismiss();
//
//                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, url + " Error: " + error.getMessage());
//                if ( pd != null)
//                    pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                String column = "", value = "";
                Map<String, String> params = new HashMap<String, String>();

                Set set = hmp.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext())
                {
                    Map.Entry me = (Map.Entry) iterator.next();
                    column = me.getKey().toString() ;
                    value = me.getValue().toString();
                    params.put(column,value);
                }
                params = CGlobals_dp.getInstance(context).getBasicMobileParams(params,
                        url, context);
//                params.put("memberemail", params.get("email"));
                Log.d(TAG, "PARAM IS " + params);
                return CGlobals_dp.getInstance(context).checkParams(params);
            }
        };
        CGlobals_dp.getInstance(context).getRequestQueue(context).add(postRequest);

    }
}
