package com.jumpinjumpout.www.customerprofile.customerprofile.ui;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.jumpinjumpout.www.customerprofile.R;

import lib.app.util.ProfileActivity;

/**
 * Created by asmita on 15-06-2015.
 */
public class CustomerProfile extends ProfileActivity {

    private String[] arrTrasportMode = {"How do you currently commute to work?","Private Vehicle (2-wheeler/4-wheeler)",
            "Auto Rickshaw / Taxi", "Radio Taxi (Ola, Uber, Meru, Tabcab etc.)", "Public Transport (Bus/Train/Metro)", "Other (Shared cars, office shuttles etc.)"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        svParent.setBackgroundResource(R.drawable.frame);
        ViewGroup.LayoutParams params = btnDone.getLayoutParams();
        params.width = ViewGroup.LayoutParams.FILL_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        btnDone.setLayoutParams(params);
        setVisibility();
        setHint();
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CustomerProfile.this, "work in progress.. ", Toast.LENGTH_LONG).show();

            }
        });
        ArrayAdapter adapterViolationType = new ArrayAdapter<String>(CustomerProfile.this, android.R.layout.simple_spinner_item, arrTrasportMode);
        adapterViolationType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTypeOffence.setAdapter(adapterViolationType);

    }

    private void setVisibility() {
        spnSubTypeOffence.setVisibility(View.GONE);
        spnTransportMode.setVisibility(View.VISIBLE);
        spnTypeOffence.setVisibility(View.GONE);
        imageLayout.setVisibility(View.GONE);
        takephoto.setVisibility(View.GONE);
        etEmailId.setVisibility(View.VISIBLE);
        etLicenceNo.setVisibility(View.GONE);
        btnDone.setVisibility(View.VISIBLE);
        etHomeLocation.setVisibility(View.VISIBLE);
        etWorkLocation.setVisibility(View.VISIBLE);
    }

    private void setHint() {
        etName.setHint(R.string.customer_name);
        etPhoneNo.setHint(R.string.phone_no);
        etEmailId.setHint(R.string.email);
        etHomeLocation.setHint(R.string.homeLoaction);
        etWorkLocation.setHint(R.string.workLocation);
    }
}
