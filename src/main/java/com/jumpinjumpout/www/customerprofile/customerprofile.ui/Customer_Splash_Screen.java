package com.jumpinjumpout.www.customerprofile.customerprofile.ui;

import android.content.Intent;
import android.os.Bundle;

import com.jumpinjumpout.www.customerprofile.R;

import lib.app.util.SplashScreen;

/**
 * Created by asmita on 15-06-2015.
 */
public class Customer_Splash_Screen extends SplashScreen {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentLayout.setBackgroundResource(R.drawable.frame);
        Intent intent = new Intent(Customer_Splash_Screen.this, CustomerProfile.class);
        startActivity(intent);
        Customer_Splash_Screen.this.finish();
    }
}
