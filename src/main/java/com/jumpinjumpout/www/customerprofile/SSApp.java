package com.jumpinjumpout.www.customerprofile;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import lib.app.util.ACRAPostSender;
import lib.app.util.CallHome;
import lib.app.util.SSLog;
import lib.app.util.UserInfo;


@ReportsCrashes(formKey = "", // will not be used
		formUri = "http://www.smartshehar.com/dev/alpha/dashboard/svr/apk/v1/php/acra/acra_error.php",
		formUriBasicAuthLogin = "yourlogin", // optional
		formUriBasicAuthPassword = "y0uRpa$$w0rd", // optional
		mode = ReportingInteractionMode.TOAST,
		resToastText = R.string.crash_toast_text)
public class SSApp extends Application {
	private static boolean mIfAppInited;
	public String msProduct;
	public String msManufacturer;
	public String msCarrier;
	public String mAppNameCode;
	private static SSApp sInstance;
	public static String mIMEI;
	public static String mGmail = null	;
	public static String mLine1Number;
	public static Location mFakeLocation;

	private Activity mActivity;

	private SQLiteDatabase mDB;
	public CallHome mCH;
	public PackageInfo mPackageInfo;
	private String mAppVersion;
	public Location mCurrentLocation;
	private Location mOldLocation;
	public ProgressDialog mProgressDialog;

	public static final String TAG = "SSC";
	private static final int HAVEGPRS = 1;
	private static final int HAVEWIFI = 2;
	public static final int REQUEST_CODE = 11;
	public static final String PHP_PATH = "http://www.smartshehar.com/svr";
	public UserInfo mUserInfo;
	SharedPreferences.Editor  mEditorVersionPersistent = null;
	private SharedPreferences mPrefs = null, mPrefsVersionPersistent = null;
	public static RequestQueue mVolleyRequestQueue;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onTerminate() {
		// this.mDB.close();
		super.onTerminate();
	}


	public void init(Activity activity) {
		ACRA.init(this);
		String currentVersion;
//		gAnalyticsId = activity.getString(R.string.ganalyticsid);
		SharedPreferences prefs;
		if (mIfAppInited)
			return;

		mFakeLocation = new Location("fake");	// Dadar - Tilak Bridge
		double lat = 19.02078;
		double lon = 72.843168;
		mFakeLocation.setLatitude(lat);
		mFakeLocation.setLongitude(lon);

		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		mAppNameCode = getString(R.string.appNameShort);
		mIMEI = telephonyManager.getDeviceId();
		msCarrier = telephonyManager.getNetworkOperator();
		mLine1Number = telephonyManager.getLine1Number();
		mLine1Number = telephonyManager.getSubscriberId();
		msProduct = Build.PRODUCT;
		msManufacturer = Build.MANUFACTURER;
//        prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
//        currentVersion = prefs.getString("dbVersionInfo", "-1");
		mActivity = activity;
		mIfAppInited = true;
		mPackageInfo = getPackageInfo();
		mUserInfo = new UserInfo(activity, getString(R.string.appTitle));


		HashMap<String, String> ACRAData = new HashMap<String, String>();
		AccountManager manager = (AccountManager) this
				.getSystemService(Context.ACCOUNT_SERVICE);
		Account[] list = manager.getAccounts();
		String msGmail = null;

		for (Account account : list) {
			if (account.type.equalsIgnoreCase("com.google")) {
				msGmail = account.name;
				break;
			}
		}

		ACRAData.put("email", msGmail);
		ACRAPostSender acraPostSender = new ACRAPostSender(ACRAData);
		ACRA.getErrorReporter().setReportSender(acraPostSender);
		Log.d("MyApplication", "onCreate()");
		sInstance = this;


		mAppVersion = mPackageInfo.versionName + mPackageInfo.versionCode;
		prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
		currentVersion = prefs.getString("dbVersionInfo", "-1");

		mCH = new CallHome(activity, getMyLocation(),
				mPackageInfo, getString(R.string.appNameShort),
				getString(R.string.appCode), mUserInfo);
		if(!currentVersion.equals(mAppVersion)) { // First Use
			mCH.userPing(getString(R.string.atFirstUse), "First Use");
		}
		readPreferences();
	}

	public static synchronized SSApp getInstance() {
		return sInstance;
	}
	public SharedPreferences.Editor getPersistentPreferenceEditor() {
		if (mEditorVersionPersistent == null) {
			mEditorVersionPersistent = getPersistentPreference().edit();
		}
		return mEditorVersionPersistent;
	}
	public SharedPreferences getPersistentPreference() {
		if (mPrefsVersionPersistent == null) {
			mPrefsVersionPersistent = getApplicationContext()
					.getSharedPreferences(
							Constants_dp.PREFS_VERSION_PERSISTENT,
							Context.MODE_PRIVATE);
		}
		return mPrefsVersionPersistent;
	}
	public void cleanUp() {
		if (mDB != null)
			mDB.close();

	}

	private PackageInfo getPackageInfo() {
		PackageInfo pi = null;
		try {
			pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return pi;
	}

	public CharSequence readAsset(String asset, Activity activity) {
		BufferedReader in = null;

		try {
			in = new BufferedReader(new InputStreamReader(activity.getAssets()
					.open(asset)));
			String line;
			StringBuilder buffer = new StringBuilder();
			while ((line = in.readLine()) != null) {
				buffer.append(line).append('\n');
			}
			return buffer;
		} catch (IOException e) {
			return "";
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				return "";
			}

		}
	}

	// Update the location
	public Location getMyLocation() {
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location loc = null;
		List<String> providers = lm.getAllProviders();
		for (String provider : providers) {
			loc = lm.getLastKnownLocation(provider);
			if (loc != null) {
				if (isBetterLocation(loc, mCurrentLocation)) {
					mCurrentLocation = loc;
					if(mOldLocation != null && mCurrentLocation != null) {
						float[] res = new float[1];
						Location.distanceBetween(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(),
								mOldLocation.getLatitude(),
								mOldLocation.getLongitude(), res);
						if(res[0] > 200 && !mCurrentLocation.getProvider().toString().equals("fake"))
							mOldLocation = mCurrentLocation;
					} else {
						if(mCurrentLocation!= null && !mCurrentLocation.getProvider().toString().equals("fake"))
							mOldLocation = mCurrentLocation;
					}
				}
			}
		}
		return mCurrentLocation;
	}

	private static final int TWO_MINUTES = 1000 * 60 * 2;

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}


	public String showDistance(double di) {
		if (di == -1)
			return " N/A";
		if (di > 1000)
			return Double.toString((int) di / 1000.00) + " km";
		else
			return Integer.toString((int) di) + " m";
	}

	/*
	 * public boolean inCityBoundingRect(StopInfo si) { ; boolean bInLat =
	 * false, bInLon = false; if(si.lat >= MINCITYLAT && si.lat <= MAXCITYLAT)
	 * bInLat = true; if(si.lon > MINCITYLON && si.lon < MAXCITYLON) bInLon =
	 * true; return bInLat && bInLon; }
	 */

	public boolean haveWiFi() {
		if (haveNetworkConnection() == HAVEWIFI)
			return true;
		return false;
	}

	public boolean haveGPRS() {
		if (haveNetworkConnection() == HAVEGPRS)
			return true;
		return false;
	}

	public int haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi)
			return HAVEWIFI;
		if (haveConnectedMobile)
			return HAVEGPRS;
		return 0;
	}

	// Read App global preferences
	public void readPreferences() {
//		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
//		bRegistered = settings.getBoolean(PREFREGISTERED, false);

	}

	// Save App global preferences
	public void savePreferences() {
//		SharedPreferences settings = getSharedPreferences(SSTApp.PREFS_NAME, 0);
//		SharedPreferences.Editor editor = settings.edit();

//		editor.commit();

	};


	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 *
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	protected boolean isBetterLocation(Location location,
									   Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is newer or older
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		boolean isNewer = timeDelta > 0;

		// If it's been more than two minutes since the current location, use
		// the new location
		// because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
			// If the new location is more than two minutes older, it must be
			// worse
		} else if (isSignificantlyOlder) {
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}

	public void showGPSDisabledAlertToUser(Activity activity) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder
				.setMessage(R.string.gps_enable)
				.setCancelable(false)
				.setPositiveButton(R.string.no_gps_message,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent callGPSSettingIntent = new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								mActivity.startActivityForResult(
										callGPSSettingIntent, REQUEST_CODE);
							}
						});
		alertDialogBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	public void addVolleyRequest(StringRequest postRequest,
								 boolean clearBeforeQuery) {
		try {
			postRequest.setRetryPolicy(new DefaultRetryPolicy(
					DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 2,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			if (clearBeforeQuery) {
				getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
					@Override
					public boolean apply(Request<?> request) {
						return true;
					}
				});
			}
			getRequestQueue().add(postRequest);
		} catch (Exception e) {
			SSLog.e(TAG, "mVolleyRequestQueue may not be initialized - ", e);
		}
	}

	public void addVolleyRequest(JsonArrayRequest postRequest,
								 boolean clearBeforeQuery) {
		clearBeforeQuery = false; // disabled for testing.
		try {
			postRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			if (clearBeforeQuery) {
				getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
					@Override
					public boolean apply(Request<?> request) {
						return true;
					}
				});
			}
			getRequestQueue().add(postRequest);
		} catch (Exception e) {
			SSLog.e(TAG, "mVolleyRequestQueue may not be initialized - ", e);
		}
	}
	public RequestQueue getRequestQueue() {
		// lazy initialize the request queue, the queue instance will be
		// created when it is accessed for the first time
		if (mVolleyRequestQueue == null) {

			// Instantiate the cache
			Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB
			// cap

			// Set up the network to use HttpURLConnection as the HTTP client.
			Network network = new BasicNetwork(new HurlStack());

			// Instantiate the RequestQueue with the cache and network.
			mVolleyRequestQueue = new RequestQueue(cache, network);
			mVolleyRequestQueue.start();
			mVolleyRequestQueue = Volley
					.newRequestQueue(getApplicationContext());

			// mVolleyRequestQueue =
			// Volley.newRequestQueue(sInstance.getApplicationContext());

			// Start the queue
			// mVolleyRequestQueue =
			// Volley.newRequestQueue(getApplicationContext());
		}

		return mVolleyRequestQueue;
	}

} // SSTApp