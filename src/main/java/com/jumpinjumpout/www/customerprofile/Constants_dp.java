package com.jumpinjumpout.www.customerprofile;

/**
 * Created by ctemkar on 02/04/2015.
 */
public class Constants_dp {

    public static final String PREF_PHONENO = "PREF_PHONENO";
    public static final String PREF_COUNTRY_CODE = "PREF_COUNTRY_CODE";
    public static final String PREFS_DP_APP_SHARED_PREFERENCE = "PREFS_DP_APP_SHARED_PREFERENCE";
    public static final String PREF_APPUSERID = "PREF_APPUSERID";
    public static final String PREF_REG_ID = "PREF_REG_ID";
    public static final String PREF_PROPERTY_APP_VERSION_NAME = "PREF_PROPERTY_APP_VERSION_NAME";
    public static final String PREF_CURRENT_COUNTRY = "PREF_CURRENT_COUNTRY";
    public static final String PREF_USER_TYPE = "PREF_USER_TYPE";
    public static final String PREF_REGISTERED = "PREF_REGISTERED";
    public static final String PREF_MY_LOCATION = "PREF_MY_LOCATION";
    public static final int TWO_MINUTES = 1000 * 60 * 2;

    public static final String VERSION = "v1";
    public static final String APPNAME = "jijoetaxiapp";
    public static final String ALPHA = "/dev/alpha";
    // public static final String ALPHA = "/dev/beta";
// public static final String ALPHA = "/prod";
    public static final String AUTHORITY = "www.jumpinjumpout.com";
    public static final String SITE = "http://" + AUTHORITY;
    // public static final String SITE = "http://hp-ferrari/sites";
    public static final String SITE_DIRECTORY = SITE + ALPHA + "/" + APPNAME;
    public static final String SVR_DIRECTORY = SITE_DIRECTORY + "/svr/apk/"
            + VERSION + "/";
    public static final String PHP_DIRECTORY = ALPHA + "/" + APPNAME
            + "/svr/apk/" + VERSION + "/php/";
    public static final String PHP_PATH = SITE + PHP_DIRECTORY;

    public static final String USER_ACCESS_URL = PHP_PATH+"driver/je_user_access.php";
    public static final String PREFS_VERSION_PERSISTENT = "PREFS_VERSION_PERSISTENT";

    public static String DRIVER_DETAILS_URL =PHP_PATH+"driver/add_driver_details.php";
    public static String INSERT_DRIVER_IMAGES_URL =PHP_PATH+"driver/driver_image.php";
    public static String DRIVER_LIST_URL = PHP_PATH+"driver/retrieve_driver_details.php";
    public static String DRIVER_IMAGES_URL = PHP_PATH+"driver/images/";
}
