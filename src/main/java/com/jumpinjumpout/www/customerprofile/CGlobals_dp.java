package com.jumpinjumpout.www.customerprofile;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import lib.app.util.SSLog;
import lib.app.util.UserInfo;

/**
 * Created by ctemkar on 02/04/2015.
 */
public class CGlobals_dp {
    public static final String TAG = "CGlobals_dp: ";
    private static CGlobals_dp instance;
    public static RequestQueue mVolleyRequestQueue;
    private static String msGmail;
    public UserInfo mUserInfo;
    public static boolean mbRegistered;
    Location mCurrentLocation ;
    Map<String, String> mMobileParams;
    public static String msPhoneNo = null;
    public static String msCountryCode = null;
    public static boolean mbAppInited;
    public boolean bStopsRead;
    protected double mdTravelDistance;
    public static String mAppNameShort;
    public static Location mFakeLocation;
    public String mAppNameCode;
    String msAndroidReleaseVersion;
    int miAndroidSdkVersion;
    public static String mIMEI;
    public String msCarrier;
    public static String mLine1Number;
    public String msProduct;
    public String msManufacturer;
    public PackageInfo mPackageInfo = null;
    public static String msAppVersionName;
    public static boolean mbIsAdmin;
    public Stack<Integer> stackActivity;
    String mCurrentVersion;
    public static int miAppVersionCode;
    private CGlobals_dp(Context context) {
        msGmail = null;

        try {
            AccountManager manager = (AccountManager) context
                    .getSystemService(Context.ACCOUNT_SERVICE);
            Account[] list = manager.getAccounts();
            for (Account account : list) {
                if (account.type.equalsIgnoreCase("com.google")) {
                    msGmail = account.name;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // private static long mlMSElapsed = 0;
    private SharedPreferences mPrefs = null, mPrefsVersionPersistent = null;
    SharedPreferences.Editor mEditor = null, mEditorVersionPersistent = null;

    public static synchronized CGlobals_dp getInstance(Context context) {
        if (instance == null) {
            instance = new CGlobals_dp(context);
        }
        return instance;
    }

    public void init(Context context) {
        // ACRA.init((Application)this);
        // Calendar c = Calendar.getInstance();
        // mlMSElapsed = c.getTimeInMillis();


        // mSelectedCity = new CCity("Mumbai", 18.579235, 72.568094,
        // 19.663942, 73.381082);
        msPhoneNo = SSApp.getInstance().getPersistentPreference()
                .getString(Constants_dp.PREF_PHONENO, "");
        msCountryCode = SSApp.getInstance().getPersistentPreference()
                .getString(Constants_dp.PREF_COUNTRY_CODE, "");

        mbAppInited = false;
        bStopsRead = false;
        mdTravelDistance = 0;

        if (mbAppInited)
            return;

        mAppNameShort = context.getString(R.string.appNameShort);
        mFakeLocation = new Location("fake"); // Dadar - Tilak Bridge
        double lat = 19.02078;
        double lon = 72.843168;
        mFakeLocation.setLatitude(lat);
        mFakeLocation.setLongitude(lon);
        mFakeLocation.setAccuracy(9999);

        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        mAppNameCode = context.getString(R.string.appNameShort);
        msAndroidReleaseVersion = Build.VERSION.RELEASE; // e.g.
        // myVersion
        // := "1.6"
        miAndroidSdkVersion = Build.VERSION.SDK_INT; // e.g.
        // sdkVersion :=
        // 8;


        mIMEI = telephonyManager.getDeviceId();
        msCarrier = telephonyManager.getNetworkOperator();
        mLine1Number = telephonyManager.getLine1Number();
        mLine1Number = telephonyManager.getSubscriberId();
        msProduct = Build.PRODUCT;
        msManufacturer = Build.MANUFACTURER;
        mbAppInited = true;
        mPackageInfo = getPackageInfo(context);
        stackActivity = new Stack<Integer>();
        AccountManager manager = (AccountManager) context
                .getSystemService(Context.ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        msGmail = null;

        for (Account account : list) {
            if (account.type.equalsIgnoreCase("com.google")) {
                msGmail = account.name;
                break;
            }
        }


        mbIsAdmin = isAdmin(msGmail);
        msAppVersionName = mPackageInfo.versionName;
        miAppVersionCode = mPackageInfo.versionCode;
        String currentVersion = SSApp.getInstance()
                .getPersistentPreference().getString("dbVersionInfo", "-1");
        if (!msAppVersionName.equals(currentVersion)) {

            SSApp.getInstance().getPersistentPreferenceEditor()
                    .putString("dbVersionInfo", msAppVersionName);
            SSApp.getInstance().getPersistentPreferenceEditor().commit();

            SSApp.getInstance().getPersistentPreferenceEditor()
                    .putString("dbVersionInfo", currentVersion);

            SSApp.getInstance().getPersistentPreferenceEditor()
                    .commit();

        }

        mCurrentVersion = SSApp.getInstance().getPersistentPreference()
                .getString("dbVersionInfo", "-1");
        mUserInfo = new UserInfo(context,
                context.getString(R.string.app_label));
        // Is this the first use after download? then send to server as first
        // use
        if (!currentVersion.equals(mPackageInfo.versionCode)) { // First Use
        }


        mMobileParams = new HashMap<String, String>();
    }

    public RequestQueue getRequestQueue(Context context) {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mVolleyRequestQueue == null) {

            // Instantiate the cache
            Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB
            // cap

            // Set up the network to use HttpURLConnection as the HTTP client.
            Network network = new BasicNetwork(new HurlStack());

            // Instantiate the RequestQueue with the cache and network.
            mVolleyRequestQueue = new RequestQueue(cache, network);
            mVolleyRequestQueue.start();

            // mVolleyRequestQueue =
            // Volley.newRequestQueue(sInstance.getApplicationContext());

            // Start the queue
            // mVolleyRequestQueue =
            // Volley.newRequestQueue(getApplicationContext());
        }

        return mVolleyRequestQueue;
    }
    private PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;
        try {
            pi = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi;
    }
    public Location getBestLocation(Context context) {
        Location gpslocation = getLocationByProvider(
                LocationManager.GPS_PROVIDER, context);
        Location networkLocation = getLocationByProvider(
                LocationManager.NETWORK_PROVIDER, context);
        // if we have only one location available, the choice is easy
        if (gpslocation == null) {
            Log.d(TAG, "No GPS Location available.");
            return networkLocation;
        }
        if (networkLocation == null) {
            Log.d(TAG, "No Network Location available");
            return gpslocation;
        }
        // a locationupdate is considered 'old' if its older than the configured
        // update interval. this means, we didn't get a
        // update from this provider since the last check
        long old = System.currentTimeMillis() - getGPSCheckMilliSecsFromPrefs();
        boolean gpsIsOld = (gpslocation.getTime() < old);
        boolean networkIsOld = (networkLocation.getTime() < old);
        // gps is current and available, gps is better than network
        if (!gpsIsOld) {
            Log.d(TAG, "Returning current GPS Location");
            return gpslocation;
        }
        // gps is old, we can't trust it. use network location
        if (!networkIsOld) {
            Log.d(TAG, "GPS is old, Network is current, returning network");
            return networkLocation;
        }
        // both are old return the newer of those two
        if (gpslocation.getTime() > networkLocation.getTime()) {
            Log.d(TAG, "Both are old, returning gps(newer)");
            return gpslocation;
        } else {
            Log.d(TAG, "Both are old, returning network(newer)");
            return networkLocation;
        }
    }
    /**
     * get the last known location from a specific provider (network/gps)
     */
    private Location getLocationByProvider(String provider, Context context) {
        Location location = null;
        if (context != null) {
            LocationManager locationManager = (LocationManager) context
                    .getApplicationContext().getSystemService(
                            Context.LOCATION_SERVICE);
            try {
                if (locationManager.isProviderEnabled(provider)) {
                    location = locationManager.getLastKnownLocation(provider);
                }
            } catch (IllegalArgumentException e) {
                Log.d(TAG, "Cannot acces Provider " + provider);
            }
        }
        return location;
    }

    int getGPSCheckMilliSecsFromPrefs() {
        return 1000;
    }
    public SharedPreferences getSharedPreference(Context context) {
        if (mPrefsVersionPersistent == null) {
            mPrefsVersionPersistent = context.getSharedPreferences(
                    Constants_dp.PREFS_DP_APP_SHARED_PREFERENCE,
                    Context.MODE_PRIVATE);
        }
        return mPrefsVersionPersistent;
    }

    public SharedPreferences.Editor getPersistentPreferenceEditor(
            Context context) {
        if (mEditorVersionPersistent == null) {
            mEditorVersionPersistent = getSharedPreference(context).edit();
        }
        return mEditorVersionPersistent;
    }

    public Location getMyLocation(Context context) {
        if (mCurrentLocation == null) {
            Location location = getBestLocation(context);
            mCurrentLocation = location; // isBetterLocation(location,
            // mCurrentLocation) ? location :
            // mCurrentLocation;
            if (mCurrentLocation == null) {
                // Gson gson = new Gson();
                String jsonLocation = SSApp.getInstance()
                        .getPersistentPreference()
                        .getString(Constants_dp.PREF_MY_LOCATION, "");
                if (!TextUtils.isEmpty(jsonLocation)) {
                    try {
                        // mCurrentLocation = gson.fromJson(jsonLocation,
                        // Location.class);
                        mCurrentLocation = locationFromJsonString(jsonLocation);
                    } catch (Exception e) {
                        SSLog.e(TAG,
                                "getMyLocation: Could not convert jsonLocation to location object",
                                e);
                        mCurrentLocation = null;

                    }
                }
            }
        }
        if (mCurrentLocation != null) {
            setMyLocation(mCurrentLocation, false);
        }
        return mCurrentLocation;
    }
    private Location locationFromJsonString(String sJsonLoc) {
        Location loc = null;
        try {
            JSONObject jo = new JSONObject(sJsonLoc);
            loc = new Location(jo.getString("provider"));
            loc.setAccuracy((float) jo.getDouble("accuracy"));
            loc.setLatitude(jo.getDouble("latitude"));
            loc.setLongitude(jo.getDouble("longitude"));
            loc.setSpeed((float) jo.getDouble("speed"));
        } catch (JSONException je) {
            SSLog.e(TAG, "locationFromJsonString", je);
        }
        return loc;
    }
    public static String getVolleyError(Context context, VolleyError error) {
        // int statusCode = error.networkResponse.statusCode;
        // NetworkResponse response = error.networkResponse;

        // Log.d("testerror",""+statusCode+" "+response.data);
        // Handle your error types accordingly.For Timeout & No connection
        // error, you can show 'retry' button.
        // For AuthFailure, you can re login with user credentials.
        // For ClientError, 400 & 401, Errors happening on client side when
        // sending api request.
        // In this case you can check how client is forming the api and debug
        // accordingly.
        // For ServerError 5xx, you can do retry or handle accordingly.
        if (error instanceof NetworkError) {
            return context.getString(R.string.retry_internet);
        } else if (error instanceof ServerError) {
        } else if (error instanceof AuthFailureError) {
        } else if (error instanceof ParseError) {
        } else if (error instanceof NoConnectionError) {
        } else if (error instanceof TimeoutError) {
        } else {
            return error.getMessage();
        }
        return "";
    }
    public boolean isBetterLocation(Location location,
                                    Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }
        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > Constants_dp.TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -Constants_dp.TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use
        // the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be
            // worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
                .getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate
                && isFromSameProvider) {
            return true;
        }
        return false;
    }
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public void setMyLocation(Location location, boolean bForceLocation) {
        if (location == null)
            return;
        if (bForceLocation) {
            mCurrentLocation = location;
        } else {
            if (isBetterLocation(location, mCurrentLocation)) {
                mCurrentLocation = location;
            }
        }
        final String jsonLocation = locationToJsonString(mCurrentLocation);
        new Thread(new Runnable() {
            public void run() {
                SSApp
                        .getInstance()
                        .getPersistentPreferenceEditor()
                        .putString(Constants_dp.PREF_MY_LOCATION,
                                jsonLocation);
                SSApp.getInstance().getPersistentPreferenceEditor()
                        .commit();
            }
        }).start();
    }
    private String locationToJsonString(Location loc) {
//        JsonObject jo = new JsonObject();
//        jo.addProperty("latitude", loc.getLatitude());
//        jo.addProperty("longitude", loc.getLongitude());
//        jo.addProperty("speed", loc.getSpeed());
//
//        jo.addProperty("provider", loc.getProvider());
//        jo.addProperty("accuracy", loc.getAccuracy());

        return "";

    }
    public Map<String, String> getBasicMobileParams(Map<String, String> params,
                                                    String url, Context context) {
        init(SSApp.getInstance().getApplicationContext());
        Location myLocation = getBestLocation(context);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        Calendar cal = Calendar.getInstance();
//        msPhoneNo = getPersistentPreference(context).getString(
//                Constants_lib.PREF_PHONENO, "");
//        msCountryCode = getPersistentPreference(context).getString(
//                Constants_lib.PREF_COUNTRY_CODE, "");
//        params.put("countrycode", msCountryCode.trim());
//        params.put("phoneno", msPhoneNo.trim());
        params.put(
                "appuserid",
                Integer.toString(getPersistentPreference(context).getInt(Constants_dp.PREF_APPUSERID, -1)));
        params.put("email", msGmail);
        params.put("clientdatetime", df.format(cal.getTime()));
        if (myLocation != null) {
            Date locationDateTime = new Date(myLocation.getTime());
            params.put("provider", myLocation.getProvider());
            params.put("lat", String.format("%.9f", myLocation.getLatitude()));
            params.put("lng", String.format("%.9f", myLocation.getLongitude()));
            params.put("accuracy", Double.toString(myLocation.getAccuracy()));
            params.put("altitude", Double.toString(myLocation.getAltitude()));
            params.put("bearing", Double.toString(myLocation.getBearing()));
            params.put("speed", Double.toString(myLocation.getSpeed()));
            params.put("locationdatetime", df.format(locationDateTime));
            params.put("provider", myLocation.getProvider());
        }
        params.put("app", mAppNameCode);
        params.put("module", "SAR");
        params.put("version", CGlobals_dp.msAppVersionName);
        params.put("android_release_version", msAndroidReleaseVersion);
        params.put("versioncode", String.valueOf(miAppVersionCode));
        params.put("android_sdk_version", Integer.toString(miAndroidSdkVersion));
        params.put("imei", CGlobals_dp.mIMEI);
        params.put("email", CGlobals_dp.msGmail);
        params.put("carrier", msCarrier);
        params.put("product", msProduct);
        params.put("manufacturer", msManufacturer);

        String delim = "";
        StringBuilder getParams = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            getParams.append(delim + entry.getKey() + "=" + entry.getValue());
            delim = "&";

        }
        try {
            url = url + "?" + getParams.toString() + "&verbose=Y";
        } catch (Exception e) {
            Log.e(TAG, e.getStackTrace().toString());
        }
        return params;
    }
    public Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it
                    .next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
                Log.d(TAG, pairs.getKey());
            }
        }
        return map;
    }
    public SharedPreferences getPersistentPreference(Context context) {
        if (mPrefsVersionPersistent == null) {
            mPrefsVersionPersistent =context
                    .getSharedPreferences(
                            Constants_dp.PREFS_VERSION_PERSISTENT,
                            Context.MODE_PRIVATE);
        }
        return mPrefsVersionPersistent;
    }
    public boolean isAdmin(String emailid) {
        String string = "ctemkar@gmail.com;priteshpanchigar@gmail.com;sultanakhanam2010@gmail.com";
        String[] splits = string.split(";");
        for (String email : splits) {
            if (email.equals(emailid)) {
                return true;
            }

        }
        return false;
    }

}
